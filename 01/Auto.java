package cz.vutbr.feec.tin.pr01;

public class Auto {
	public int price;
	public String znacka;
	public Auto vlavo;
	
	Auto(int price, String znacka){
		this.price = price;
		this.znacka = znacka;
	}

	@Override
	public String toString() {
		return "" + price + " " + znacka;
	}
	
	public Auto sused() {
		return vlavo;
	}
	
	
}
