package cz.vutbr.feec.tin.pr01;

public class Main {

	public static void main(String[] args) {
		Auto t1 = new Auto(100, "Skoda");
		Auto t2 = new Auto(500, "Audi");
		Auto t4 = new Auto(300, "BMW");
		
		Auto t3 = t1;
		
		t1.vlavo = t2;
		t2.vlavo = t4;
		
		System.out.println(t1);
		System.out.println(t2);
		System.out.println(t3);
		System.out.println(t4);
		
		System.out.println("-------------------------------------");
		
		System.out.println(t1.sused());
		System.out.println(t2.sused());
		System.out.println(t3.sused());
		System.out.println(t4.sused());
	}

}
