package cz.vutbr.feec.tin.pr09;

public class LinearnyZoznam {
	private Node prvy = null;
	private Node posledny = null;

	public Node getPrvy() {
		return prvy;
	}

	public void setPrvy(Node prvy) {
		this.prvy = prvy;
	}
	
	public Node getPosledny() {
		return posledny;
	}

	public void setPosledny(Node posledny) {
		this.posledny = posledny;
	}
	
	public void pridajNaZacatek(int data) {
			/*Node tmp = new Node(data);
			tmp.setDalsi(prvy);
			this.prvy = tmp;*/
		Node tmp = new Node(data);
		tmp.setDalsi(null);
		tmp.setPredchadzajuci(null);
		
		if(prvy == null)
		{
			prvy = tmp;
		}else {
			prvy.setPredchadzajuci(tmp);
			tmp.setDalsi(prvy);
			prvy = tmp;
		}
		
	}
	
	public void odstranZoZaciatku() {
		/*Node tmp = prvy.getDalsi();
		this.prvy = tmp;*/
		if(prvy!=null) {
			Node tmp = prvy;
			prvy = prvy.getDalsi();
			tmp = null;
			if(prvy!=null) {
				prvy.setPredchadzajuci(null);
			}
		}
	}
	
	public boolean obsahuje(int data) {
		Node tmp = prvy;
		
		while(tmp!=null) {
			if(tmp.getData()==data) {
				return true;
			}
			tmp = tmp.getDalsi();
		}
		return false;
	}
	
	public void printList() {
		Node tmp =  prvy;
		if(tmp!=null) {
			System.out.println("Zoznam obsahuje: ");
			while(tmp!=null) {
				System.out.print(tmp.getData() + " ");
				tmp = tmp.getDalsi();
			}
			System.out.println("\n");
		}
		else {
			System.out.println("Zoznam je prazdny\n");
		}
	}
	
	public void reversePrint() {
		Node tmp = prvy;
		if(tmp!=null) {
			System.out.println("Zoznam obsahuje: ");
			while(tmp.getDalsi()!=null) {
				tmp = tmp.getDalsi();
			}
			
			while(tmp!=prvy) {
				System.out.print(tmp.getData() + " ");
				tmp = tmp.getPredchadzajuci();
			}
			System.out.println(tmp.getData() + "\n");
		}
		else {
			System.out.println("Zoznam je prazdny\n");
		}
		
	}
}
