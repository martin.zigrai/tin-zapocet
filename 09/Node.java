package cz.vutbr.feec.tin.pr09;

public class Node {
	private Node dalsi;
	private Node predchadzajuci;
	private int data;
	
	public Node(int data) {
		this.data = data;
	}
	
	public Node getDalsi() {
		return dalsi;
	}
	
	public void setDalsi(Node dalsi) {
		this.dalsi = dalsi;
	}
	
	public int getData() {
		return data;
	}
	
	public void setData(int data) {
		this.data = data;
	}

	public Node getPredchadzajuci() {
		return predchadzajuci;
	}

	public void setPredchadzajuci(Node predchadzajuci) {
		this.predchadzajuci = predchadzajuci;
	}
	
}
