package cz.vutbr.feec.tin.pr02;

import java.util.LinkedList;

public class Mapa {
	private LinkedList<Auto> auta = new LinkedList<Auto>();
	
	public void pridajAuto(Auto a) {
		auta.add(a);
	}
	
	public void vypis() {
		for(Auto a: auta)
		{
			System.out.println(a);
		}
	}
}
