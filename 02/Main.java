package cz.vutbr.feec.tin.pr02;


public class Main {

	public static void main(String[] args) {
		Mapa m = new Mapa();
		
		Auto t1 = new Auto("Skoda", 10, 10);
		Auto t2 = new Auto("Audi", 54, 69);
		Auto t3 = new Auto("BMW", 2, 18);
		Auto t4 = new Auto("Mercedes", 84, 12);
		
		m.pridajAuto(t1);
		m.pridajAuto(t2);
		m.pridajAuto(t3);
		m.pridajAuto(t4);
		
		m.vypis();
	}

}
