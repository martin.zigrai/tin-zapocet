package cz.vutbr.feec.tin.pr02;

public class Auto {
	private int polohaX;
	private int polohaY;
	private String znacka;
	
	Auto(String znacka, int polohaX, int polohaY){
		this.znacka = znacka;
		this.polohaX = polohaX;
		this.polohaY = polohaY;
	}

	public String getZnacka() {
		return znacka;
	}

	public void setZnacka(String znacka) {
		this.znacka = znacka;
	}

	public int getPolohaX() {
		return polohaX;
	}

	public void setPolohaX(int polohaX) {
		this.polohaX = polohaX;
	}

	public int getPolohaY() {
		return polohaY;
	}


	public void setPolohaY(int polohaY) {
		this.polohaY = polohaY;
	}

	@Override
	public String toString() {
		return "" + polohaX + " " + polohaY + " " + znacka;
	}
	
	
	
}
