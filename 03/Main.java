package cz.vutbr.feec.tin.pr03;

public class Main {

	public static void main(String[] args) {
		Paket p1 = new Paket();
		Paket p2 = new Paket();
		Paket p3 = new Paket();
		Paket p4 = new Paket();
		
		p1.setPak(p2);
		p2.setPak(p3);
		p3.setPak(p4);
		
		Paket b1 = new Paket();
		Paket b2 = new Paket();
		Paket b3 = new Paket();
		Paket b4 = new Paket();
		
		b1.setPak(b2);
		b2.setPak(b3);
		b3.setPak(b4);
		b4.setPak(b1);
		
	}

}
