package cz.vutbr.feec.tin.pr111;

import java.util.ArrayList;
import java.util.HashSet;

public class Route {
	private int price;
	private ArrayList<Integer> nodes = new ArrayList<>();
	private HashSet<Edge> edges;
	
	public Route(Graph g, int value) {
		nodes.add(value);
		edges = (HashSet<Edge>) g.getEdges();
	}
	
	 public void addNode(int value){
	        nodes.add(value);
	        price = countPrice();
	    }

	
	public int getValue(int node1, int node2) {
		ArrayList<Edge> ed = new ArrayList<>(edges);
		for(int i = 0; i < ed.size(); i++) {
			if(ed.get(i).getNode1().getId() == node1 && ed.get(i).getNode2().getId() == node2) {
				return ed.get(i).getValue();
			}
		}
		return -1;
	}
	
	public int countPrice() {
		int p = 0;
		for (int i = 1; i < nodes.size(); i++) {
            p += getValue(nodes.get(i-1), nodes.get(i));
        }
        return p;

	}
	
	public int getPrice() {
		return price;
	}
	
	@Override
	public String toString() {
		return "" + nodes + " " + edges; 
	}
}
