package cz.vutbr.feec.tin.pr111;

public class Edge {
	private Node node1;
	private Node node2;
	private int value;
	
	public Edge(Node node1, Node node2, int value) {
		this.node1 = node1;
		this.node2 = node2;
		this.value = value;
	}

	public Node getNode1() {
		return node1;
	}

	public void setNode1(Node node1) {
		this.node1 = node1;
	}

	public Node getNode2() {
		return node2;
	}

	public void setNode2(Node node2) {
		this.node2 = node2;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return  node1 + " - " + node2 + " (" + value + ")" ;
	}
	
	public int compareTo(Edge compareEdge) {
		return this.value - compareEdge.value;
	}
	
	
}
