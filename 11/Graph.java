package cz.vutbr.feec.tin.pr111;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

public class Graph {
	private HashMap<Integer, Node> nodes = new HashMap<>();
	private HashSet<Edge> edges = new HashSet<>();
	
	public void addEdge(int idNode1, int idNode2, int value) {
		Node node1 = create_or_return_node(idNode1);
		Node node2 = create_or_return_node(idNode2);
		
		Edge edge = new Edge(node1, node2, value);
		edges.add(edge);
	}
	
	private Node create_or_return_node(int idNode) {
		Node tmp = nodes.get(idNode);
		if(tmp == null) {
			tmp = new Node(idNode);
			nodes.put(idNode, tmp);
		}
		return tmp;
	}
	
	public HashSet<Edge> getEdges(){
		return edges;
	}
	
	public HashMap<Integer, Node> getNodes(){
		return nodes;
	}
	
	public LinkedList<Edge> getSortEdges(){
		HashSet<Edge> mset = new HashSet<>();
		mset.addAll(getEdges());
		int n = mset.size();
		LinkedList<Edge> ed = new LinkedList<>();
		for(Edge x : mset) {
			ed.add(x);
		}
		
		Collections.sort(ed, new Comparator<Edge>(){
			@Override
			public int compare(Edge o1, Edge o2) {
				return o1.getValue() - o2.getValue();
			}
		});
		
		return ed;
	}

	@Override
	public String toString() {
		return "Graph{" +
                "nodes=" + nodes +
                ", edges=" + edges +
                '}';

	}
	
	
}
