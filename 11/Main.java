package cz.vutbr.feec.tin.pr111;

public class Main {

	public static void main(String[] args) {
		Graph g = new Graph();
		
		g.addEdge(1, 7, 16);
		g.addEdge(1, 6, 6);
		g.addEdge(6, 5, 5);
		g.addEdge(6, 7, 8);
		
		System.out.println(g);
		
		Route r = new Route(g, 1);
		r.addNode(6);
		r.addNode(5);
		//r.addNode(3);
		
		System.out.println(r.getPrice());
	}

}
