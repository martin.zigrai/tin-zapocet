package cz.vutbr.feec.tin.pr06;

import java.util.HashSet;

public class Main {

	public static void main(String[] args) {
		HraciePole h = new HraciePole();
		HraciePole h1 = new HraciePole();
		
		h1.posun(HraciePole.DOLE);
		
		System.out.println(h);
		System.out.println(h1);
		
		h1.posun(HraciePole.VPRAVO);
		System.out.println(h1);

		h1.posun(HraciePole.HORE);
		System.out.println(h1);
		
		h1.posun(HraciePole.VLAVO);
		System.out.println(h1);
		
		h1.posun(HraciePole.VLAVO);
		System.out.println(h1);
		
		HraciePole h2 = new HraciePole();
		
		System.out.println(h2);
		
		HashSet<HraciePole> mn = new HashSet<>();
		mn.add(h);
		mn.add(h1);
		mn.add(h2);
		
		System.out.println(mn.size());
	}

}
