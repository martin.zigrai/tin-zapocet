package cz.vutbr.feec.tin.pr06;

import java.util.Arrays;

public class HraciePole {
	private int[][] data = {{0,1,2},{3,4,5},{6,7,8}};
	private int pozX = 0;
	private int pozY = 0;
	
	
	public static final int HORE = 0;
	public static final int DOLE = 1;
	public static final int VLAVO = 2;
	public static final int VPRAVO = 3;
	
	@Override
	public String toString() {
		String res = "";
		for(int y=0;y< data[0].length;y++) {
			for(int x=0;x< data.length;x++){
				res += data[y][x]+" ";
			}
			res += "\n";
		}
		return res;
	}
	
	public void posun(int smer) {
		switch(smer) {
		case DOLE:
			if(pozY >= 2) break;
			data[pozY][pozX] = data[pozY+1][pozX];
			data[pozY + 1][pozX] = 0;
			pozY++;
			break;
		case HORE:
			if(pozY <= 0) break;
			data[pozY][pozX] = data[pozY-1][pozX];
			data[pozY - 1][pozX] = 0;
			pozY--;
			break;
		case VPRAVO:
			if(pozX >= 2) break;
			data[pozY][pozX] = data[pozY][pozX+1];
			data[pozY][pozX+1] = 0;
			pozX++;
			break;	
		case VLAVO:
			if(pozX <= 0) break;
			data[pozY][pozX] = data[pozY][pozX-1];
			data[pozY][pozX-1] = 0;
			pozX--;
			break;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(data);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HraciePole other = (HraciePole) obj;
		if (!Arrays.deepEquals(data, other.data))
			return false;
		return true;
	}
	
	
	
	
}
