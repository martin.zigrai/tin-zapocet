package cz.vutbr.feec.tin.pr04;

public class Strom {
	private Strom lavy;
	private Strom pravy;
	
	public Strom getLavy() {
		return lavy;
	}
	
	public void setLavy(Strom lavy) {
		this.lavy = lavy;
	}
	
	public Strom getPravy() {
		return pravy;
	}
	
	public void setPravy(Strom pravy) {
		this.pravy = pravy;
	}
	
}
