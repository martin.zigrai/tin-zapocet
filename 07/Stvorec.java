package cz.vutbr.feec.tin.pr07;

public class Stvorec extends Objekt {
	private int dlzka;
	
	public Stvorec(int polohaX, int polohaY, int dlzka) {
		super(polohaX, polohaY);
		this.setDlzka(dlzka);
	}

	public int getDlzka() {
		return dlzka;
	}

	public void setDlzka(int dlzka) {
		this.dlzka = dlzka;
	}

	@Override
	public String vykresli() {
		return "Vykreslujem Stvorec";
	}
	
	
}
