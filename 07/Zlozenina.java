package cz.vutbr.feec.tin.pr07;

import java.util.LinkedList;

public class Zlozenina extends Objekt{
	private LinkedList<Objekt> sucasti = new LinkedList<Objekt>();
	
	public Zlozenina(int polohaX, int polohaY) {
		super(polohaX, polohaY);
	}
	
	public void pridaj(Objekt o) {
		sucasti.add(o);
	}

	public void vytvor() {
		for(Objekt o: sucasti) {
			System.out.println(o.vykresli());
		}
	}
	
	
} 
