package cz.vutbr.feec.tin.pr07;

public class Kruh extends Objekt {
	private int polomer;
	
	public Kruh(int polohaX, int polohaY, int polomer) {
		super(polohaX, polohaY);
		this.setPolomer(polomer);
	}

	public int getPolomer() {
		return polomer;
	}

	public void setPolomer(int polomer) {
		this.polomer = polomer;
	}
	
	@Override
	public String vykresli() {
		return "Vykreslujem kruh";
	}
}
