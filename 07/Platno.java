package cz.vutbr.feec.tin.pr07;

import java.util.LinkedList;

public class Platno {
	private LinkedList<Objekt> objekty = new LinkedList<Objekt>();
	private int rozmerX;
	private int rozmerY;
	
	public Platno(int rozmerX, int rozmerY) {
		this.setRozmerX(rozmerX);
		this.setRozmerY(rozmerY);
	}
	
	public void pridaj(Objekt o) {
		objekty.add(o);
	}
	
	public String vytvorRaster(String nazov) {
		return "Vytvaram raster " + nazov;
	}

	public int getRozmerX() {
		return rozmerX;
	}

	public void setRozmerX(int rozmerX) {
		this.rozmerX = rozmerX;
	}

	public int getRozmerY() {
		return rozmerY;
	}

	public void setRozmerY(int rozmerY) {
		this.rozmerY = rozmerY;
	}
	
	public void vytvor() {
		for(Objekt o: objekty) {
			System.out.println(o.vykresli());
		}
	}
}
