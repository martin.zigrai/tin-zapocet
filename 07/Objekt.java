package cz.vutbr.feec.tin.pr07;

public abstract class Objekt {
	private int polohaX;
	private int polohaY;
	
	public Objekt(int polohaX, int polohaY) {
		this.polohaX = polohaX;
		this.polohaY = polohaY;
	}

	public int getPolohaX() {
		return polohaX;
	}

	public void setPolohaX(int polohaX) {
		this.polohaX = polohaX;
	}

	public int getPolohaY() {
		return polohaY;
	}

	public void setPolohaY(int polohaY) {
		this.polohaY = polohaY;
	}
	
	public String vykresli() {
		return "Vykreslujem objekt";
	}
}
