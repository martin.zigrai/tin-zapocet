package cz.vutbr.feec.tin.pr07;

public class Main {

	public static void main(String[] args) {
		Platno p = new Platno(800,800);
		
		p.pridaj(new Kruh(10,15,30));
		p.pridaj(new Kruh(22,4,14));
		p.pridaj(new Stvorec(50,50,30));
		
		Zlozenina z = new Zlozenina(50,50);
		z.pridaj(new Kruh(30,30,100));
		z.pridaj(new Stvorec(100,100,80));
		p.pridaj(z);
		
		System.out.println(p.vytvorRaster("mojobrazok.png"));
		
		z.vytvor();
		System.out.println("");
		p.vytvor();
		
		
	}

}
