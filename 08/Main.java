package cz.vutbr.feec.tin.pr08;

public class Main {

	public static void main(String[] args) {
		Paket p1 = new Paket("A");
		Paket p2 = new Paket("B");
		Paket p3 = new Paket("C");
		Paket p4 = new Paket("D");
		
		p1.setPacket(p2);
		p2.setPacket(p3);
		p3.setPacket(p4);
	}

}
