package cz.vutbr.feec.tin.pr08;

public class Paket {
	private Paket packet;
	private String data;
	
	public Paket(String data) {
		this.data = data;
	}
	
	public Paket getPacket() {
		return packet;
	}
	
	public void setPacket(Paket packet) {
		this.packet = packet;
	}
	
	public String getData() {
		return data;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	
}
