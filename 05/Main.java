package cz.vutbr.feec.tin.pr05;

public class Main {

	public static void main(String[] args) {
		Uzol u1 = new Uzol("u1");
		Uzol u2 = new Uzol("u2");
		Uzol u3 = new Uzol("u3");
		Uzol u4 = new Uzol("u4");
		Uzol u5 = new Uzol("u5");
		Uzol u6 = new Uzol("u6");
		
		u1.pridajUzol(u6);
		u1.pridajUzol(u4);
		u1.pridajUzol(u2);
		u1.pridajUzol(u5);
		
		u2.pridajUzol(u1);
		u2.pridajUzol(u5);
		u2.pridajUzol(u3);

		u5.pridajUzol(u4);

		u1.vypis();
		System.out.println(" ");
		u2.vypis();
		System.out.println(" ");
		u3.vypis();
		System.out.println(" ");
		u4.vypis();
		System.out.println(" ");
		u5.vypis();
		System.out.println(" ");
		u6.vypis();
	}

}
