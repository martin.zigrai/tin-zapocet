package cz.vutbr.feec.tin.pr05;

import java.util.LinkedList;

public class Uzol {
	private LinkedList<Uzol> uzol = new LinkedList<Uzol>();
	private String nazov;
	
	public Uzol(String nazov) {
		this.nazov = nazov;
	}
	
	public void pridajUzol(Uzol u) {
		uzol.add(u);
	}
	
	public void vypis() {
		for(Uzol u: uzol) {
			System.out.println(u);
		}
	}

	public String getNazov() {
		return nazov;
	}

	public void setNazov(String nazov) {
		this.nazov = nazov;
	}

	@Override
	public String toString() {
		return "" + nazov;
	}
	
}
