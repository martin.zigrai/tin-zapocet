package cz.vutbr.feec.tin.pr10;

public class Node {
	private Node lavy;
	private Node pravy;
	private int data;
	
	public Node(int data) {
		this.data = data;
	}
	
	public Node getLavy() {
		return lavy;
	}
	public void setLavy(Node lavy) {
		this.lavy = lavy;
	}
	public Node getPravy() {
		return pravy;
	}
	public void setPravy(Node pravy) {
		this.pravy = pravy;
	}
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}
	
}
