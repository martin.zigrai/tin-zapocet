package cz.vutbr.feec.tin.pr10;

public class Main {

	public static void main(String[] args) {
		Tree t = new Tree();
		
		t.addNode(10);
		t.addNode(8);
		t.addNode(13);
		t.addNode(5);
		t.addNode(9);
		/*t.addNode(10);
		t.addNode(16);
		t.addNode(13);*/
		
		/*t.find(1);
		t.find(2);
		t.find(4);
		t.find(5);
		t.find(7);
		t.find(8);
		t.find(9);
		t.find(10);
		t.find(11);
		
		System.out.println(t.findBool(1));
		System.out.println(t.findBool(2));
		System.out.println(t.findBool(4));
		System.out.println(t.findBool(5));
		System.out.println(t.findBool(7));
		System.out.println(t.findBool(8));
		System.out.println(t.findBool(9));
		System.out.println(t.findBool(10));
		System.out.println(t.findBool(11));
		*/
		t.printPreorder();
		t.printIn();
		t.printPost();
		t.printLeafNodes();
		//System.out.println(t.printDeep());
		
		t.convert();
	}

}
