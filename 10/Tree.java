package cz.vutbr.feec.tin.pr10;

public class Tree {
	private Node root;

	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}
	
	public void addNode(int data) {
		Node tmp = new Node(data);
		tmp.setLavy(null);
		tmp.setPravy(null);
		
		if(root==null) {
			root = tmp;
		}
		else {
			Node n = root;
			
			while(n!=null) {
				if(n.getData()==data)
				{
					return;
				}
				else if(n.getData() > data) {
					if(n.getLavy()==null) {
						n.setLavy(tmp);
					}
					else {
						n = n.getLavy();
					}
				}
				else {
					if(n.getPravy()==null) {
						n.setPravy(tmp);
					}
					else {
						n = n.getPravy();
					}
				}
			}
		}
	}
	
	public void find(int data) {
		Node tmp = root;
		if(tmp==null) {
			System.out.println("Tento strom je prazdny");
		}
		else {
			while(tmp!=null) {
				if(tmp.getData()==data) {
					System.out.println("Tato hodnota sa tu nachadza");
					return;
				}
				else {
					if(tmp.getData() > data )
					{
						tmp = tmp.getLavy();
					}
					else
					{
						tmp = tmp.getPravy();
					}
				}
			}
			System.out.println("Zadama hodnota sa tu nenachadza more");
		}
	}
	
	public boolean findBool(int data) {
		Node tmp = root;
		if(tmp==null) {
			return false;
		}
		else {
			while(tmp!=null) {
				if(tmp.getData()==data) {
					return true;
				}
				else {
					if(tmp.getData() > data )
					{
						tmp = tmp.getLavy();
					}
					else
					{
						tmp = tmp.getPravy();
					}
				}
			}
			return false;
		}
	}
	
	public void printPreorder() {
		Node n = root;
		System.out.print("PreOrder: ");
		printPreorder(n);
		System.out.println("\n");
	}
	
	private void printPreorder(Node n) {
		if(n == null)
		{
			return;
		}
		System.out.print(n.getData() + " ");
		printPreorder(n.getLavy());
		printPreorder(n.getPravy());
	}
	
	public void printIn() {
		Node n = root;
		System.out.print("InOrder: ");
		printIn(n);
		System.out.println("\n");
	}
	
	private void printIn(Node n) {
		if(n == null)
		{
			return;
		}
		printIn(n.getLavy());
		System.out.print(n.getData() + " ");
		printIn(n.getPravy());
	}
	
	public void printPost() {
		Node n = root;
		System.out.print("PostOrder: ");
		printPost(n);
		System.out.println("\n");
	}
	
	private void printPost(Node n) {
		if(n == null)
		{
			return;
		}
		printPost(n.getLavy());
		printPost(n.getPravy());
		System.out.print(n.getData() + " ");
	}
	
	public void printLeafNodes() {
		Node n = root;
		System.out.print("Leaf nodes: ");
		printLeafNodes(n);
		System.out.println("\n");
	}
	
	private static void printLeafNodes(Node n) {
		if(n == null) {
			return;
		}
		
		if(n.getLavy() == null && n.getPravy() == null) {
			System.out.print(n.getData() + " ");
		}
		
		printLeafNodes(n.getLavy());
		printLeafNodes(n.getPravy());
	}
	
	int deepestlevel;
	int value;
	
	public int printDeep() {
		Node n = root;
		printDeep(n, 0);
		return value;
	}
	
	private void printDeep(Node n, int level) {
		//int deepestlevel = 0;
		//int value = 0;
		if(n != null) {
			printDeep(n.getLavy(), ++level);
			if(level > deepestlevel) {
				value = n.getData();
				deepestlevel = level;
			}
			printDeep(n.getPravy(), level);
		}
		//System.out.println(value);
	}
	
	public void convert() {
		Node head = null;
		Node tmp = root;
		
		head = convert(tmp, head);
		
		head = reverse(head);
		printDLL(head);
	}
	
	public void printDLL(Node head) {
		Node tmp = head;
		while(tmp!=null) {
			System.out.println(tmp.getData() + " ");
			tmp = tmp.getPravy();
		}
	}
	
	private Node convert(Node tmp, Node head) {
		if(tmp == null) {
			return head;
		}
		
		head = convert(tmp.getLavy(), head);
		tmp.setLavy(null);
		
		Node right = tmp.getPravy();
		
		tmp.setPravy(head);
		if(head!=null) {
			head.setLavy(tmp);
		}
		
		head = tmp;
		return convert(right, head);
	}
	
	public Node reverse(Node head) {
		Node prev = null;
		Node tmp = head;
		
		while(tmp!=null) {
			Node n = tmp.getLavy();
			tmp.setLavy(tmp.getPravy());
			tmp.setPravy(n);
			
			prev = tmp;
			tmp = tmp.getLavy();
		}
		
		return prev;
	}
	
}
